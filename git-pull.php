<?php
/**
 * Inspired by:
 * 
 * - [Git WebHook PHP Post Receive Pull Method](http://www.tegdesign.com/git-webhook-php-post-receive-pull-method/)
 * - [Github WebHook processor](https://gist.github.com/843129/86c3c3c23c990c2edd78928ea05a2d02cb696031)
 **/

// path to projects on the server
define('PROJECTS_PATH', 'http://talonholsters.obndemo.com/');
// server key for authentication
define('SERVER_KEY', '70:b8:53:99:a9:84:32:c7:40:14:c8:de:cc:f8:2d:82 talonobn@v6host1.outdoorbusiness');

// bitbucket may emit a webhook that has changes to any number of branches
// this function checks to see if the push changed anything on master
function anymasterbranch($obj) {
  return ($obj->branch === 'master');
}

// parse the json payload
$input = file_get_contents("php://input");
$payload = json_decode($input);

// ignore webhooks without a relevant payload
if (!$payload) exit();
// ignore webhooks without the server key as the 'key' url parameter
if (!($_REQUEST['key'] == SERVER_KEY)) exit();
// ignore webhooks that aren't hosted on bitbucket
if (!($payload->{'canon_url'} === 'https://bitbucket.org')) exit();
// ignore webhooks that don't have any changes to master
if (0 == count(array_filter($payload->{'commits'}, "anymasterbranch"))) exit();

// the slug in the repository can be used as a safe directory name for the project
$project_name = $payload->{'repository'}->{'slug'};
// assumes there may be multiple projects hosted on the same server, under a common root
$project_directory = PROJECTS_PATH . '/' . $project_name;
// pull the git changes into project directory
shell_exec( 'cd ' . $project_directory . '/ && git reset --hard HEAD && git pull' );

?>